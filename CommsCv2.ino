#include <Servo.h>

/*
  ~COMMENTS ABOUT CODING STYLE~
  
  for the sake of readability of our code and just to generally keep it cleaner we should move towards
  using #define for constants.
  Atleast for what we can define constants (and what we generally will need to calibrate)
  for such as, motorSpeedForward/Reverse, or threshold.
  
  This means pin allocations will still be done the same as always (although it can be done the same way,
  ive just chosen to differentiate between the two).
  
  Functionally this wont make a difference but will help differentiate between declarations
  and hopefully make our code easier to read and modify.
  
  CURRENTLY the code does not reflect these changes but moving forward if you create more variables
  follow these stylistic changes. I will be changing the code to reflect these changes as soon as
  I get all incomplete functions in.


*/


//Roller golabal variables
Servo clampServo; //servo that moves the clamp
Servo beltMotor; //motor that moves the belt

int clampPin = 12; //placeholder pin numbers
int clampOnPin = 4;

int beltENA = 8; //belt PWM input
int beltIn1 = 11; //belt current direction
int beltIn2 = 12; //belt current direction

int linearENA = 9;
int linearIn1 = 13;
int linearIn2 = 14;

int motorSpeedForward = 180; //max, min and stop values in microseconds
int motorSpeedReverse = 0;
int motorStop = 90;
int clampRest = 50; //clamp rest and engaged values in microseconds
int clampEngage = 90;

//Arm movement and can counting
Servo rollerServo;
int armPin = 6; 
int countSwitch = 10; //limit switch to count the cans
int switchState = 0;
int prevSwitchState = 0;

//L-shape Global Variables
//Motor
Servo lServo;
int lMotorSpeedForward = 0; 
int lMotorSpeedReverse = 180;
int lMotorStop = 90; 
//Button
int buttonState = 0;
int button = 40;
//L-shape Sensor
const float VCC = 4.98; // Measured voltage of Ardunio 5V line
const float R_DIV = 10000.0; // Measured resistance of 3.3k resistor
const float STRAIGHT_RESISTANCE = 27200.0; // resistance when straight
const float BEND_RESISTANCE = 60000.0; //
const int sensor = A3;
float threshold = 50;
int sensorState= 0;// variable to store the value read from the sensor pin
//L-shape Switch
int Switch = 41;
int SwitchState = 0;
int previousSwitchState = 0;

//Outtake global variables
int solenoidpin = 2;    //defines solenoid @pin 4 <-- Change as required
int timer = (1000) + 5000; // Gets can count and opens the lock until for timer + 5 secs 

int robotPosition = 1; //1 is ready to collect cans, 0 is not (set to 1 for testing purposes)
int armIdentifier = 28;
bool armState = NULL; //true -> l-shape, false -> roller

void setup() {
  pinMode(armIdentifier, INPUT); //declare belt motor pin to check pulse time
  //if no pulse is dectected, we use the l-shape design
  if(digitalRead(armIdentifier) == LOW){ //CHANGE TO PIN FOR ARM
    //L-shape setup
    pinMode(button,INPUT);
    lServo.attach(6,1000,2000);
    pinMode(sensor,INPUT);
    pinMode(Switch,INPUT_PULLUP);
    armState == false;
  }
  else if(digitalRead(armIdentifier) == HIGH){ //else, use the roller setup
    //Roller setup
    //Note: The speeds and positions of motors and clamp can be changed from a 0 - 180 range rather
    //      than a 1000 - 2000 range by changing writeMicroseconds to just write and changing the motor
    //      speed, stop and clamp rest values accordingly.
    
    pinMode(clampOnPin, OUTPUT); //initialises output pins
    clampServo.attach(clampPin, 1000, 2000); //designates signal pin for clamp and its min and max speeds
    clampServo.writeMicroseconds(clampRest); //defaults clamp to resting position
    
    pinMode(beltENA, OUTPUT);
    pinMode(beltIn1, OUTPUT); 
    pinMode(beltIn2, OUTPUT);
    digitalWrite(beltIn1, HIGH); //Set initial motor direction  ***assuming this is forward for now
    diitalWrite(beltIn2, LOW); //set initial motor direction
    
    int beltFoward = 70; //probably best to replace these with defined constants.
    int motorStop = 0; 
    
    pinMode(armPin,OUTPUT); 
    pinMode(canPin,INPUT);
    rollerServo.attach(armPin,1000,2000);

    armState == true;
  }
  pinMode(linearENA, OUTPUT);
  pinMode(linearIn1, OUTPUT);
  pinMode(linearIn2, OUTPUT);
  digitalWrite(linearIn1, HIGH); ***assuming these states are for forward for now.
  digitalWrite(linearIn2, LOW);
  //Outtake setup
  pinMode(solenoidpin, OUTPUT); //sets solenoid as Output

  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(robotPosition == 1){
    getCans(10);//*example* cans to collect will be given to us
  }
}

void armExtend (){
  // assuming robots in position
  // set arm extend speec
  //
  digitalWrite(linearIn1, HIGH);
  digitalWrite(linearIn2, LOW);
  digitalWrite(linearENA, motorSpeedForward);
  
}

void armRetract(){
   digitalWrite(linearIn1, LOW);
  digitalWrite(linearIn2, HIGH);
  digitalWrite(linearENA, motorSpeedForward);
}

void armStop(){
  rollerServo.write(motorStop);
}

boolean canDetect(){ //Roller 
  if(digitalRead(canPin) == HIGH){ // Digital read gives the pulse and the can is counted. Proximity sensor
    return true;
  }
  else{
    return false;
  }
}

//Pre: The robot has positioned itself with a can in range of the belt and clamp
//Post: The clamp will clamp onto the can and hold it in place
void clamp(){
  clampServo.writeMicroseconds(clampEngage);
}

//Pre: The clamp is in the clamped position and the belt motor is off
//Post: The clamp will move back to its unclamped state
void unclamp(){
  clampServo.writeMicroseconds(clampRest);
}

//Pre: The clamp is engaged and has a can in it's grasp
//Post: The belt will run for a period of time then switch off
void beltRun(){
  analogWrite(ena, motorForward);
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
}

//Pre: The belt is running and has collected the required number of cans
//Post: The belt will stop running
void beltStop(){
  analogWrite(ena, motorStop);
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
}

int lock(){
  digitalWrite(solenoidpin, LOW);  //sets the solenoid into HIGH state
  return 1; //confirms that door has been locked
}

void unlock(){
  digitalWrite(solenoidpin, HIGH);  //sets the solenoid into HIGH state
  delay(timer);
  lock(); //auto-locks once time is up.
}

int lShape(int num){
  int numberOfCans = 0;
  
  buttonState = digitalRead(button);
  SwitchState = digitalRead(Switch);
  
  lServo.write(lMotorSpeedForward); //turns the motor On
  delay(500); // waits for onTime milliseconds
  
  int flexSensor = analogRead(sensor);
  float flexV = flexSensor * VCC / 1023.0;
  float flexR = R_DIV * (VCC / flexV - 1.0);
  // Use the calculated resistance to estimate the sensor's
  // bend angle:
  float angle = map(flexR, STRAIGHT_RESISTANCE, BEND_RESISTANCE,
                   0, 90.0);
  delay(500);
  if(angle >= threshold){ //sensors get pressed 
    numberOfCans++; //count++
  }
 
  if(buttonState || numberOfCans == num){ //buttonPressed or get enough cans
    lServo.write(lMotorStop); //motorStop
    delay(500);

    // turns the motor pin to another direction
    lServo.write(lMotorSpeedReverse);
    if (SwitchState != previousSwitchState) {
    // if the state has changed,stop motor
      if (SwitchState == HIGH) {
        lServo.write(lMotorStop);
        // waits for offTime milliseconds
        delay(500);
      }
    }
  return numberOfCans;
  }
}

int getCans(int numOfCans) {
  int count = 0;
  if(armState == true) { //roller flag
    //sets a timeout so the arm only extends for 10 seconds
    unsigned long baseTime = millis();
    unsigned long curTime = millis(); 
    armExtend();
    while((curTime - baseTime) <= 1000){
      curTime = millis();
      if ((curTime - baseTime) >= 1000) {
        armStop();
        break;
      }
    }
    //not sure if bradley still uses clampRest-might have to change
    clamp();
    if(clampServo.read() == clampRest){  //guarding beltRun function so it only runs if clamp is in engaged position
      //sets a timer for beltrun, stops after 10 seconds or when cans are counted
      //MIGHT HAVE TO CHANGE THIS--CANT THINK OF A WAY WITHOUT WHILE LOOP
      unsigned long baseTime = millis();
      unsigned long curTime = millis(); 
      beltRun;
      while((curTime - baseTime) <= 1000){
        curTime = millis();
        //when switch is pressed, increment count
        switchState = digitalRead(countSwitch);
        if(switchState != prevSwitchState) {
          if(switchState == HIGH) {
            count++;
          }
        }
        //if 10 secs has passed, or cans have been counted, break loop
        if ((curTime - baseTime) >= 1000 || count-1 == numOfCans) {
          beltStop();
          break;
        }
      } 
    }
  unclamp();
  return count;
  }
  else if(armState == false) {
    return lShape(numOfCans);
  }
}

void unload(){
  unlock();
  lock();
}

void unload(){
  unlock();
  lock();
}
